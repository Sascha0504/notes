import {defineStore} from "pinia";
import axios from "axios";

export const useNoteStore = defineStore('note', {
    state: () => ({
        notes: {}
    }),
    actions: {
        async loadNotes() {
            const response = await axios.get('/api/notes/')
            this.notes = response.data
        },
        async saveNote(noteText, noteTitle) {
            const response = await axios.post('/api/notes/', {
                "id": null,
                "header": noteTitle.value,
                "note": noteText.value
            })
            await this.loadNotes()
        },
        async deleteNote(id) {
            const response = await axios.delete('http://localhost:8080/api/notes/' + id)
            await this.loadNotes()

        }
    }})