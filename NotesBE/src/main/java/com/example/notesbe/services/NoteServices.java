package com.example.notesbe.services;


import com.example.notesbe.entities.NoteEntity;
import com.example.notesbe.repositories.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class NoteServices {
    @Autowired
    NoteRepository noteRepository;

    public NoteEntity addNote(NoteEntity noteEntity) {
        noteRepository.save(noteEntity);
        return noteEntity;
    }

    public List<NoteEntity> getNotes() {
        return (List<NoteEntity>) noteRepository.findAll();
    }

    public void deleteNote(int id) throws Exception{
        if(noteRepository.existsById(id))
            noteRepository.deleteById(id);
        else
            throw new Exception("Note id not found");
    }
}
