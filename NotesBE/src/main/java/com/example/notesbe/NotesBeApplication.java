package com.example.notesbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotesBeApplication {

    public static void main(String[] args) {
        SpringApplication.run(NotesBeApplication.class, args);
    }

}
