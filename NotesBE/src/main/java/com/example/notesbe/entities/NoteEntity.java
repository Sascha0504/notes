package com.example.notesbe.entities;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity(name ="Note")
@Getter
@Setter
@NoArgsConstructor
public class NoteEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private int id;

    @Column(name = "note", nullable = false, columnDefinition = "Text")
    String note;

    @Column(name = "header", nullable = false)
    String header;

    public NoteEntity(String note, String header) {
        this.note = note;
        this.header = header;
    }

    public int getId(){
        return id;
    }
}
