package com.example.notesbe.controller;

import com.example.notesbe.entities.NoteEntity;
import com.example.notesbe.services.NoteServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.http.HttpResponse;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/notes")
public class NoteController {
    @Autowired
    NoteServices noteServices;


    @PostMapping
    public ResponseEntity createNote(@RequestBody NoteEntity note) {

        return ResponseEntity.ok(noteServices.addNote(note));
    }

    @GetMapping
    public ResponseEntity getNotes() {
        return ResponseEntity.ok(noteServices.getNotes());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteNote(@PathVariable int id) {
        try {
            noteServices.deleteNote(id);
            return ResponseEntity.ok("Notiz gelöscht");
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


}
